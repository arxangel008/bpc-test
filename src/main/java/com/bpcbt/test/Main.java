package com.bpcbt.test;

import java.util.List;

import static com.bpcbt.test.UnluckyVassal.printReportForKing;
import static java.util.Arrays.asList;

/**
 * Основной класс
 */
public class Main {

    /**
     * Список от подчиненных, переданные королю
     */
    private static final List<String> POLL_RESULTS = asList(
            "служанка Аня",
            "управляющий Семен Семеныч: крестьянин Федя, доярка Нюра",
            "дворянин Кузькин: управляющий Семен Семеныч, жена Кузькина, экономка Лидия Федоровна",
            "экономка Лидия Федоровна: дворник Гена, служанка Аня",
            "доярка Нюра",
            "кот Василий: человеческая особь Катя",
            "дворник Гена: посыльный Тошка",
            "киллер Гена",
            "зажиточный холоп: крестьянка Таня",
            "секретарь короля: зажиточный холоп, шпион Т",
            "шпион Т: кучер Д",
            "посыльный Тошка: кот Василий",
            "аристократ Клаус",
            "просветленный Антон"
    );

    public static void main(String... args) {
        printReportForKing(POLL_RESULTS);
    }
}

