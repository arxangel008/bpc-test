package com.bpcbt.test;

import java.util.List;

import static java.util.Arrays.stream;

/**
 * Невезучий слуга, которому пришлось разгребать за королем
 * "Вот уйду к другому королю!!! Пусть сам выкручивается!"
 */
public class UnluckyVassal {

    /**
     * Формирование иерархии подчиненных короля и вывод
     *
     * @param pollResults список подчиненных со слугами
     */
    public static void printReportForKing(List<String> pollResults) {
        TreeNode king = new TreeNode("Король");

        pollResults.forEach(p -> {
            String[] recordsSplit = p.split(":");
            TreeNode subordinate = king.addChild(recordsSplit[0]);

            if (recordsSplit.length > 1) {
                stream(recordsSplit[1].split(",")).forEach(s -> subordinate.addChild(s.trim()));
            }
        });

        System.out.println(king);
    }
}
