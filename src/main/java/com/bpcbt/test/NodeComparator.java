package com.bpcbt.test;

import java.util.Comparator;

/**
 * Компаратор списка узлов
 */
public class NodeComparator implements Comparator<TreeNode> {

    @Override
    public int compare(TreeNode o1, TreeNode o2) {
        return o1.getData().compareToIgnoreCase(o2.getData());
    }
}
