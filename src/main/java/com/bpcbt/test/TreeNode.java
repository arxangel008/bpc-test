package com.bpcbt.test;


import java.util.SortedSet;
import java.util.TreeSet;

import static java.lang.String.join;
import static java.util.Collections.nCopies;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;

/**
 * Дерево
 */
public class TreeNode {

    private TreeNode parent;
    private final String data;
    private final SortedSet<TreeNode> children;

    public TreeNode(String data) {
        this.data = data;
        this.children = new TreeSet<>(new NodeComparator());
    }

    /**
     * Получение значение узла
     */
    public String getData() {
        return data;
    }

    /**
     * Получение уровня вложенности узла
     */
    public int getLevel() {
        return ofNullable(parent)
                .map(p -> parent.getLevel() + 1)
                .orElse(0);
    }

    /**
     * Добавление узла в дерево
     *
     * @param child Строковое представление узла
     */
    public TreeNode addChild(String child) {
        return checkNodeAndAddChild(new TreeNode(child));
    }

    /**
     * Переопределенное строковое представление дерева
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(join("", nCopies(getLevel(), "\t")));
        stringBuilder.append(data);

        if (!children.isEmpty()) {
            stringBuilder.append("\n");
            stringBuilder.append(children.stream()
                    .map(TreeNode::toString)
                    .collect(joining("\n")));
        }

        return stringBuilder.toString();
    }


    /**
     * Проверка и добавление узла
     *
     * @param childNode узел
     */
    private TreeNode checkNodeAndAddChild(TreeNode childNode) {
        childNode.parent = this;

        TreeNode node = findNodeByData(childNode);

        if (isNull(node)) {
            this.children.add(childNode);
            return childNode;
        }

        return changeParentAndChildren(childNode, node);
    }

    /**
     * Смена зависимостей родителя и детей у узла
     *
     * @param childNode вставляемый узел
     * @param node      найденный узел
     */
    private TreeNode changeParentAndChildren(TreeNode childNode, TreeNode node) {
        if (node.getLevel() == 1) {
            if (node.children.isEmpty()) {
                node.parent.children.remove(node);
                this.children.add(childNode);
                return childNode;
            } else {
                node.parent.children.remove(node);
                node.parent = this;
                this.children.add(node);
                return node;
            }
        }

        return node;
    }


    private TreeNode findNodeByData(TreeNode node) {
        TreeNode rootNode = getRootNode(node);

        return findNode(rootNode, node);
    }

    /**
     * Поиск узла
     *
     * @param parentNode   родительский узел
     * @param requiredNode искомый узел
     */
    private TreeNode findNode(TreeNode parentNode, TreeNode requiredNode) {
        if (parentNode.data.equals(requiredNode.data)) {
            return parentNode;
        }

        for (TreeNode child : parentNode.children) {
            TreeNode node = findNode(child, requiredNode);

            if (nonNull(node)) {
                return (node);
            }
        }

        return null;
    }

    /**
     * Получение корня дерева
     *
     * @param node текущий узел
     */
    private TreeNode getRootNode(TreeNode node) {
        return ofNullable(node.parent)
                .map(this::getRootNode)
                .orElse(node);
    }
}
